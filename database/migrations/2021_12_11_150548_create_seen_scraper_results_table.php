<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeenScraperResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seen_scraper_results', function (Blueprint $table) {
            $table->id();
            $table->string('text', 100);
            $table->string('image', 100);
            $table->string('region', 50);
            $table->string('attributes', 255);
            $table->string('link', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seen_scraper_results');
    }
}
