
//toggle theme
$("#ToggleThemeButton").click(function()
{
    console.log('clicked btn');
    //work in progress
});

//sets default theme
const html = document.querySelector('html');
html.dataset.theme = `theme-light`;

//toggles beetween themes
var ThemeButton = document.getElementById("ToggleThemeButton");
ThemeButton.addEventListener("click", function()
{
    if(html.dataset.theme == "theme-light")
    {
        html.dataset.theme = `theme-dark`;
        console.log('set to dark');
    }
    else
    {
        html.dataset.theme = `theme-light`;
        console.log('set to light');
    }
},
false);


//Prevent multiple tabs //used to work
localStorage.openpages = Date.now();
var onLocalStorageEvent = function(e)
{
    if(e.key == "openpages")
    {
        localStorage.page_available = Date.now();
    }
    if(e.key == "page_available")
    {
        window.location.href = "tab-warning"
    }
};
window.addEventListener('storage', onLocalStorageEvent, false);