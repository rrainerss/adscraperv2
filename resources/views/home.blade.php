@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ __('Welcome to AdScraper') }}
                </div>

                <div class="d-flex justify-content-center">
                    <a href="{{ url('scraper') }}"><button class="CustomButton mx-4 my-4">Scraper</button></a>
                    <a href="{{ url('settings') }}"><button class="CustomButton mx-4 my-4">Settings</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
