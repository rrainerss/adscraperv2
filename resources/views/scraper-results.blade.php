@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @foreach($results as $result)
            <div class="card mb-4">
                <div class="card-body px-0">

                    <div class="row my-4 d-flex justify-content-center">
                        <img class="col col-2 mx-0" src="{{$result['image']}}">
                        <div class="TextCol bg-success col-9 mx-0">
                            <b>{{$result['text']}}...</b>
                        </div>
                    </div>

                    <div class="row my-4 d-flex justify-content-center"> 
                        <div class="col-11">
                            <a href="https://www.ss.com/msg/{{$result['link']}}.html"><button class="CustomButton">Link</button></a>
                            <a href=""><button class="CustomButton">Mark as seen</button></a>
                            @if(empty($result['region']))
                            @else
                                <b>Region: 
                                    {{$result['region']}}
                                </b>
                            @endif  
                            
                        </div>
                    </div>

                    <div class="row my-4 d-flex justify-content-center">
                        @foreach (json_decode($result['attributes']) as $key => $attr)
                            <div class="col-2 text-center">
                                <b>{{ $key }}:</b>
                                <p>{{ $attr }}</p>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        @endforeach   

    </div>
</div>

@endsection