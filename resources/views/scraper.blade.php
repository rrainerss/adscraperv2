@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center">
                    <form class="w-100" id="QuickUrlForm" method="POST" action="{{url('scraper/request')}}" method="post">
                        @csrf
                        <h5 class="text-center">Please enter an <b>ss.com</b> link</h5>
                        <h5>A valid link contains several visible listings and ends with a /</h5>
                        <br>
                        <h5>Example:</h5>
                        <br>
                        <h5><b>https://www.ss.com/lv/electronics/computers/pc/</b></h5>
                        <br>
                        <input class="w-100 form-control" id="QuickLinkInput" name="QuickLinkInput" placeholder="Paste your link here" type="url" required>
                        <button type="submit" class="CustomButton mt-4">Get results</button>
                    </form>
                    <a href="{{ url('home') }}"><button class="CustomButton mx-4 my-4">Return Home</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection
