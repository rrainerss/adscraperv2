@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="text-center">Settings</h5>
                    <a href="{{ url('home') }}"><button class="CustomButton mx-4 my-4">Return Home</button></a>
                    <a><button class="CustomButton mx-4 my-4" id="ToggleThemeButton">Toggle theme</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection