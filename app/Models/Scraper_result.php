<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Scraper_result extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = 
    [
        'text',
        'image',
        'region',
        'attributes',
        'link',
        'users_id'
    ];
}
