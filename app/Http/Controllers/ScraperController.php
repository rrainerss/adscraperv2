<?php

namespace App\Http\Controllers;

use Goutte\Client;
use Goutte;
use Illuminate\Http\Request;
use App\Models\Scraper_result;
use Auth;
use DB;

class ScraperController extends Controller
{
    //initiates arrays
    public $results = array();
    public $colnames = array();


    public function ToggleTheme()
    {
        dd('toggle theme');
    }


    public function MarkAsSeen()
    {
        dd('markasseen');
    }


    public function GetData()
    {
        $data = Scraper_result::orderBy('id')->where('users_id',Auth::id())->get();
        return view('scraper-results', ['results'=>$data]);
    }

    
    public function Scraper(Request $request)
    {
        //gets input link
        $url = $request->input('QuickLinkInput');
        //few other variables
        $urlref = $url;
        $pagenr = 0;
        $scrapedelementcount = 0;
        //client instance
        $client = new Client();

        function ScraperLogic($request, $url, $urlref, $scrapedelementcount, $client, $pagenr)
        {
            $pagenr++;  //iterates page on every loop
            $url = $urlref.'page'.$pagenr.'.html';      //appends string to link for each page
            $crawler = $client->request('GET', $url);   //get request to url

            $pagecount = $crawler->filterXPath('//*[@class="navi"]')->count() - 1;  //gives page count of category

            global $results;    //
            global $colnames;   //arrays
            
            usleep(50000);  //small delay between requests

            //gets column names for attributes
            $colcount = $crawler->filterXPath('//*[@id="filter_frm"]/table[2]//*[@class="msg_column_td"]')->count();
            for($i = 0; $i < $colcount; $i++)
            {
                $colnames[$i] = $crawler->filterXPath('//*[@id="filter_frm"]/table[2]//*[@class="msg_column_td"]')->eq($i)->text();
            }   

            //gets content of each listing (30 or less tr elements)
            $listingelement = $crawler->filterXPath('//*[@id="filter_frm"]/table[2]//tr[position()>1 and position()<last()]');
            //counts the elements
            $listingcount = $listingelement->count();
            
            //gets all element data from page
            for($iter2 = $scrapedelementcount; $iter2 < ($listingcount + $scrapedelementcount); $iter2++)
            {
                //gets text from element
                $results[$iter2]['text'] = $listingelement->eq($iter2-$scrapedelementcount)->filter('.msg2')->filter('.d1')->filter('a')->text();
                //gets image link from element
                $results[$iter2]['image'] = $listingelement->eq($iter2-$scrapedelementcount)->filter('.msga2')->filter('a')->filter('img')->attr('src');
                //checks if region exists in page, sometimes it doesnt
                if($listingelement->eq($iter2-$scrapedelementcount)->filter('.msg2')->filter('.ads_region')->count() != 0)
                {
                    //gets region from element
                    $results[$iter2]['region'] = $listingelement->eq($iter2-$scrapedelementcount)->filter('.msg2')->filter('.ads_region')->text();
                }
                else
                {
                    //sets region value to null if doesnt exist
                    $results[$iter2]['region'] = null;
                }
                //gets the listing link/id from element
                $linkelement = $listingelement->eq($iter2-$scrapedelementcount)->filter('.msg2')->filter('.d1')->filter('a')->attr('href');
                //removes all characters except the identifying letters
                $lastslash = strripos($linkelement, '/', 1) + 1;
                $lastperiod = strripos($linkelement, '.', 1);
                $linkend = substr($linkelement, $lastslash, 99);
                $linkid = substr($linkend, 0, -5);
                //saves letters to array
                $results[$iter2]['link'] = $linkid;
                //gets all attribute elements, counts them
                $attrelement = $listingelement->eq($iter2-$scrapedelementcount)->filter('.pp6');
                $attrcount = $attrelement->count();
                for($iter3 = 0; $iter3 < $attrcount; $iter3++)
                {
                    //saves each attribute to array
                    $results[$iter2]['attributes'][$colnames[$iter3]] = $attrelement->eq($iter3)->text();
                }
            }
            //increases count of elements that have been acquired
            $scrapedelementcount = $scrapedelementcount + 30;

            //if not on last page already, keep looping
            if($pagenr < $pagecount)
            {
                ScraperLogic($request, $url, $urlref, $scrapedelementcount, $client, $pagenr);
            }
            else
            {
                //if on last page, small delay
                usleep(50000);

                //get current logged in used id
                $id = Auth::id();
                //delete all previous temporary records with user id
                DB::table('scraper_results')->where('users_id', $id)->delete();
                //iterate over each result and save to database
                foreach($results as $res)
                {
                    $savedata = new Scraper_result;
                    $savedata->text = $res['text'];
                    $savedata->image = $res['image'];
                    $savedata->region = $res['region'];
                    $savedata->attributes = json_encode($res['attributes']);
                    $savedata->link = $res['link'];
                    $savedata->users_id = Auth::id();
                    $savedata->save();
                }
                //redirect to result page
                return redirect('scraper/results')->send();
            }
        }
        //run the function
        ScraperLogic($request, $url, $urlref, $scrapedelementcount, $client, $pagenr);
    }
}