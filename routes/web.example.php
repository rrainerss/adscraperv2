<?php

use Illuminate\Support\Facades\Route;
//include controllers?

Route::get('/', function(){
    return view('welcome');
});

Route::get('scraper', [App\Http\Controllers\ScraperController::class, 'scraper'])->name('scraper');
