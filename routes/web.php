<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScraperController;

Route::group(['middleware' => ['auth']], function ()
{
    Route::get('/', function(){return redirect('home');});
    Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

    Route::get('scraper', function(){return view('scraper');})->name('scraper');
    Route::get('settings', function(){return view('settings');})->name('settings');
    Route::get('scraper/request', function(){return redirect('scraper');});
    Route::post('scraper/request', 'App\Http\Controllers\ScraperController@Scraper')->name('scraper-request');
    Route::get('scraper/results', 'App\Http\Controllers\ScraperController@GetData')->name('scraper-results');
    Route::get('scraper/results/mark-as-seen', 'App\Http\Controllers\ScraperController@MarkAsSeen')->name('scraper-results-mark-as-seen');

    Route::get('tab-warning', function(){return view('tab-warning');})->name('tab-warning');
});

Auth::routes();